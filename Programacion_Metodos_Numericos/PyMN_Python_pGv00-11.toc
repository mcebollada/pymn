\select@language {spanish}
\thispagestyle {empty}
\contentsline {chapter}{\'{I}ndice de figuras}{\es@scroman {viii}}{chapter*.1}
\contentsline {chapter}{\'{I}ndice de cuadros}{\es@scroman {ix}}{chapter*.2}
\contentsline {chapter}{\IeC {\'I}ndice c\IeC {\'o}digos fuente}{\es@scroman {xi}}{chapter*.3}
\contentsline {part}{I\hspace {1em}Primeros pasos}{3}{part.1}
\contentsline {chapter}{La base del lenguaje Python}{5}{chapter.1.1}
\contentsline {section}{Introducci\IeC {\'o}n}{5}{section*.5}
\contentsline {section}{Un lenguaje y varios paradigmas}{5}{section*.6}
\contentsline {section}{Como obtengo e instalo Python}{6}{section*.7}
\contentsline {section}{Usando Python}{7}{section*.8}
\contentsline {subsection}{Modo interactivo}{7}{section*.9}
\contentsline {subsection}{Ejecuci\IeC {\'o}n de archivos}{7}{section*.10}
\contentsline {part}{II\hspace {1em}A programar con Python}{13}{part.2}
\contentsline {chapter}{Introducci\IeC {\'o}n}{15}{chapter.2.1}
\contentsline {section}{Tareas}{15}{section*.11}
\contentsline {subsection}{Ejercicios de algoritmos}{17}{section*.12}
\contentsline {chapter}{Tipos de Datos}{19}{chapter.2.2}
\contentsline {section}{Tipos de Datos Num\IeC {\'e}ricos}{19}{section*.13}
\contentsline {subsection}{Tipo int (Entero)}{20}{section*.14}
\contentsline {subsection}{Tipo float (Punto Flotante)}{22}{section*.15}
\contentsline {subsection}{Tipo complex (Complejo)}{22}{section*.16}
\contentsline {subsection}{Tipo bool (l\IeC {\'o}gico)}{22}{section*.17}
\contentsline {subsection}{Ejercicios de n\IeC {\'u}meros}{25}{section*.18}
\contentsline {section}{Tipos de Datos Secuencia y Colecciones}{26}{section*.19}
\contentsline {subsection}{Tipo str}{26}{section*.20}
\contentsline {subsection}{Ejercicios de cadenas}{28}{section*.21}
\contentsline {subsection}{Tipo list y tuple (conjuntos, colecciones o ``bag'')}{28}{section*.22}
\contentsline {subsection}{Ejercicios de listas y tuplas}{29}{section*.23}
\contentsline {subsection}{M\IeC {\'a}s Colecciones}{31}{section*.24}
\contentsline {subsection}{Ejercicios de colecciones}{32}{section*.25}
\contentsline {chapter}{Operadores Simples y Funciones}{35}{chapter.2.3}
\contentsline {section}{Operadores aplicados a n\IeC {\'u}meros}{35}{section*.26}
\contentsline {subsection}{Ejercicios de operadores num\IeC {\'e}ricos}{36}{section*.27}
\contentsline {section}{Operadores L\IeC {\'o}gicos}{37}{section*.28}
\contentsline {subsection}{Operadores sobre tipos L\IeC {\'o}gicos}{37}{section*.29}
\contentsline {subsection}{Operadores con resultado L\IeC {\'o}gico}{38}{section*.30}
\contentsline {subsection}{Ejercicios con operadores L\IeC {\'o}gicos}{39}{section*.31}
\contentsline {section}{Operadores aplicados a Secuencias}{40}{section*.32}
\contentsline {subsection}{Ejercicios con operadores aplicados a secuencias}{40}{section*.33}
\contentsline {section}{Operador de asignaci\IeC {\'o}n y Declaraci\IeC {\'o}n de variables}{41}{section*.34}
\contentsline {subsection}{Ejercicios declaraci\IeC {\'o}n de variables}{42}{section*.35}
\contentsline {section}{Funciones incorporadas}{43}{section*.36}
\contentsline {subsection}{Ejercicios de funciones}{45}{section*.37}
\contentsline {chapter}{Instrucciones control de flujo de programa}{47}{chapter.2.4}
\contentsline {section}{instrucci\IeC {\'o}n ``if''}{47}{section*.38}
\contentsline {section}{Instrucciones de repetici\IeC {\'o}n}{50}{section*.39}
\contentsline {subsection}{Instrucci\IeC {\'o}n ``while''}{51}{section*.40}
\contentsline {subsection}{Instrucci\IeC {\'o}n ``for''}{52}{section*.41}
\contentsline {section}{Funciones de Usuario}{55}{section*.42}
\contentsline {subsection}{Generalidades}{56}{section*.43}
\contentsline {subsection}{Funciones}{57}{section*.44}
\contentsline {subsection}{Procedimientos}{57}{section*.45}
\contentsline {subsection}{Par\IeC {\'a}metros}{57}{section*.46}
\contentsline {subsection}{Alcance de las variables}{58}{section*.47}
\contentsline {subsubsection}{Importante}{58}{section*.48}
\contentsline {chapter}{Paquetes de python para c\IeC {\'a}lculo y graficaci\IeC {\'o}n}{61}{chapter.2.5}
\contentsline {section}{Los recursos de numpy}{62}{section*.49}
\contentsline {section}{Los recursos de matplotlib}{65}{section*.50}
\contentsline {subsection}{Usos de ``plot''.}{66}{section*.51}
\contentsline {subsection}{Usos de ``scatter, bar, pie, hist y boxplot''.}{69}{section*.52}
\contentsline {section}{Otros recursos}{69}{section*.53}
\contentsline {subsection}{Los recursos de scipy}{69}{section*.54}
\contentsline {subsection}{Los recursos de simpy}{72}{section*.55}
\contentsline {subsection}{Los recursos de pandas}{72}{section*.56}
\contentsline {part}{III\hspace {1em}Los m\IeC {\'e}todos num\IeC {\'e}ricos}{73}{part.3}
\contentsline {chapter}{Error}{77}{chapter.3.1}
\contentsline {section}{Ejercicios}{77}{section*.57}
\contentsline {chapter}{Soluci\IeC {\'o}n de ecuaciones lineales}{79}{chapter.3.2}
\contentsline {section}{M\IeC {\'e}todos directos}{79}{section*.58}
\contentsline {subsection}{Eliminaci\IeC {\'o}n de Gauss}{79}{section*.59}
\contentsline {subsection}{Estrategia de pivote}{79}{section*.60}
\contentsline {subsection}{Factorizacion}{79}{section*.61}
\contentsline {section}{M\IeC {\'e}todos iterativos}{79}{section*.62}
\contentsline {subsection}{Jacobi y Gauss-Seidel}{79}{section*.63}
\contentsline {subsection}{Convergencia}{79}{section*.64}
\contentsline {section}{Ejercicios}{79}{section*.65}
\contentsline {chapter}{Soluci\IeC {\'o}n de ecuaciones no lineales}{81}{chapter.3.3}
\contentsline {section}{M\IeC {\'e}todo de bisecci\IeC {\'o}n}{81}{section*.66}
\contentsline {section}{M\IeC {\'e}todo de Newton}{82}{section*.67}
\contentsline {section}{M\IeC {\'e}todo de la secante}{83}{section*.68}
\contentsline {section}{Raices de polinomios}{85}{section*.69}
\contentsline {section}{Ejemplos}{85}{section*.70}
\contentsline {subsection}{1}{85}{section*.71}
\contentsline {subsection}{2}{86}{section*.72}
\contentsline {section}{Ejercicios}{91}{section*.73}
\contentsline {chapter}{Aproximaci\IeC {\'o}n de funciones y datos}{93}{chapter.3.4}
\contentsline {section}{Interpolaci\IeC {\'o}n}{93}{section*.74}
\contentsline {subsection}{Polin\IeC {\'o}mica de Lagrange}{93}{section*.75}
\contentsline {subsection}{Con funciones splines}{93}{section*.76}
\contentsline {section}{M\IeC {\'\i }nimos cuadrados}{93}{section*.77}
\contentsline {subsection}{Discreta}{93}{section*.78}
\contentsline {subsection}{Continua}{93}{section*.79}
\contentsline {section}{Ejercicios}{93}{section*.80}
\contentsline {chapter}{Derivaci\IeC {\'o}n e integraci\IeC {\'o}n num\IeC {\'e}ricas}{95}{chapter.3.5}
\contentsline {section}{Derivaci\IeC {\'o}n num\IeC {\'e}rica}{95}{section*.81}
\contentsline {subsection}{Derivadas primeras}{95}{section*.82}
\contentsline {subsection}{Derivadas segundas}{95}{section*.83}
\contentsline {subsection}{El error en la derivada num\IeC {\'e}rica}{95}{section*.84}
\contentsline {section}{Integraci\IeC {\'o}n num\IeC {\'e}rica}{95}{section*.85}
\contentsline {subsection}{Reglas de cuadratura b\IeC {\'a}sicas}{95}{section*.86}
\contentsline {subsection}{Reglas de cuadratura compuestas}{95}{section*.87}
\contentsline {subsection}{Cuadratura gaussiana}{95}{section*.88}
\contentsline {section}{Ejercicios}{95}{section*.89}
\contentsline {chapter}{Resoluci\IeC {\'o}n num\IeC {\'e}rica de problemas de valor inicial}{97}{chapter.3.6}
\contentsline {section}{Problema de Cauchy}{97}{section*.90}
\contentsline {section}{M\IeC {\'e}todos de Taylor}{97}{section*.91}
\contentsline {section}{M\IeC {\'e}todos de Runge-Kutta}{97}{section*.92}
\contentsline {section}{M\IeC {\'e}todos multipaso}{97}{section*.93}
\contentsline {subsection}{Expl\IeC {\'\i }citos de Adams-Bashforth}{97}{section*.94}
\contentsline {subsection}{Impl\IeC {\'\i }citos de Adams-Moulton}{97}{section*.95}
\contentsline {subsection}{Predictor-Corrector}{97}{section*.96}
\contentsline {section}{Ecuaciones de orden superior y sistemas de ecuaciones diferenciales}{97}{section*.97}
\contentsline {section}{Ejercicios}{97}{section*.98}
\contentsline {chapter}{Resoluci\IeC {\'o}n num\IeC {\'e}rica de PVF en dos puntos}{99}{chapter.3.7}
\contentsline {section}{PVF e segundo orden en dos puntos}{99}{section*.99}
\contentsline {section}{M\IeC {\'e}todo de disparo lineal}{99}{section*.100}
\contentsline {section}{M\IeC {\'e}todos de diferencias finitas lineales}{99}{section*.101}
\contentsline {section}{M\IeC {\'e}todo de disparo no lineal}{99}{section*.102}
\contentsline {section}{M\IeC {\'e}todos de diferencias finitas no lineales}{99}{section*.103}
\contentsline {section}{Ejercicios}{99}{section*.104}
\contentsline {chapter}{M\IeC {\'e}todos num\IeC {\'e}ricos para ecuaciones en derivadas parciales}{101}{chapter.3.8}
\contentsline {section}{Diferencias finitas para ecuaciones el\IeC {\'\i }pticas}{101}{section*.105}
\contentsline {section}{Diferencias finitas para ecuaciones parab\IeC {\'o}licas}{101}{section*.106}
\contentsline {section}{Diferencias finitas para ecuaciones hiperb\IeC {\'o}licas}{101}{section*.107}
\contentsline {section}{Introducci\IeC {\'o}n al m\IeC {\'e}todo de los elementos finitos}{101}{section*.108}
\contentsline {section}{Ejercicios}{101}{section*.109}
\contentsline {part}{IV\hspace {1em}\IeC {\'I}ndices y Anexos}{103}{part.4}
\contentsline {chapter}{Anexos}{105}{section*.110}
\contentsline {chapter}{Anexo I: Detalles de los objetos de Python}{107}{appendix.Alph1}
\contentsline {section}{N\IeC {\'u}meros}{107}{section*.111}
\contentsline {subsection}{Enteros ``clase int''}{107}{section*.112}
\contentsline {subsection}{Reales ``clase float''}{110}{section*.113}
\contentsline {subsection}{Complejos ``clase complex''}{111}{section*.114}
\contentsline {subsection}{M\IeC {\'o}dulo ``number''}{112}{section*.115}
\contentsline {subsubsection}{Clase abstracta base ``number.Number''}{112}{section*.116}
\contentsline {subsubsection}{Clase abstracta ``number.Complex''}{113}{section*.117}
\contentsline {subsubsection}{Clase abstracta ``number.Real''}{113}{section*.118}
\contentsline {subsubsection}{Clase abstracta base ``number.Rational''}{114}{section*.119}
\contentsline {subsubsection}{Clase abstracta base ``number.Integral''}{114}{section*.120}
\contentsline {subsubsection}{Clase ``decimal.Decimal''}{114}{section*.121}
\contentsline {subsubsection}{Clase ``fractions.Fraction''}{126}{section*.122}
\contentsline {section}{Iterables}{129}{section*.123}
\contentsline {subsection}{Cadenas ``clase str''}{129}{section*.124}
\contentsline {subsection}{Conjuntos ``clase list''}{129}{section*.125}
\contentsline {subsection}{Conjuntos ``clase tuple''}{129}{section*.126}
\contentsline {subsection}{Conjuntos ``clase set''}{129}{section*.127}
\contentsline {subsection}{Conjuntos ``clase frozenset''}{129}{section*.128}
\contentsline {subsection}{Diccionarios ``clase dict''}{129}{section*.129}
\contentsline {chapter}{\IeC {\'I}ndice alfab\IeC {\'e}tico}{129}{section*.129}
